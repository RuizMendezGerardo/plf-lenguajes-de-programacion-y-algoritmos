```mermaid
graph TB
  subgraph "Video 1"
  Titulo[Programando ordenadores en los 80 y ahora] --> actuales[Sistemas actuales]
  Titulo --> antiguos[Sistemas antiguos]
  actuales --> def1[Sistemas que ya no<br> estan a la venta]
  antiguos --> def2[Sistemas modernos<br> usados diariamente]

  actuales --> car1[Caracteristicas]
  antiguos --> car2[Caracteristicas]

  car1 --> carAc1[Mayor potencia]
  car1 --> carAc2[Lenguajes de alto<br>nivel]
  car1 --> carAc3[Busca la claridad<br>de codigo]
  carAc2 --> carAc[Sobrecarga en<br>llamadas a librerías]

  car2 --> carAn1[Mayor variedad<br>entre sistemas]
  car2 --> carAn2[Lenguajes de bajo<br>nivel]
  car2 --> carAn3[Busca tener<br>código útil]
  carAn3 --> carAn4[Enfoque total en el codigo]
  carAn1 --> carAn5[Necesitas conocer<br>la arquitectura del sistema]
end
```
```mermaid
graph TB
  subgraph "Video 2"
  Titulo[Historia de los algoritmos<br> y de los lenguajes de programación] --> algoritmo[Algoritmos]
  Titulo --> lenguajes[Lenguajes de programacion]
  algoritmo --> def1[Procedimientos sistemáticos<br>y mecánicos]
  lenguajes --> def2[Instrumento para comunicas<br>un algoritmo a una maquina]

  algoritmo --> carAl1[Importancia]
  algoritmo --> carAl2[Comportamiento]
  lenguajes --> carLe1[Paradigmas actuales]
  lenguajes --> carLe2[Limites]

  carAl1 --> al1[Resuelven problemas<br>rapidamente]
  carAl2 --> al2[Con un dato de entrada calcula<br>un dato de salida]
  carLe1 --> le1[Imperativo]
  carLe1 --> le2[POO]
  carLe2 --> le3[No puede comprobar<br>formulas lógicas]
  carLe2 --> le4[No puede calcular<br>su tiempo ni costo de ejecución]

  le1 --> l1[c]
  le1 --> l2[fortran]
  le2 --> l3[java]
  le2 --> l4[c++]
end
```